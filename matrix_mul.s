

// A[i][k], sum, B[k][j], C[i][j] --> IEEE double precision format : stored inside the Coprocessor in S0-S31.
             
N:   .word 16   // put the (32-bit integer) #128 into the address location "N"
zero: .double 0.0 // 8-byte wide


A:
    .word 0x0
    .fill 256, 8, 0x40140000        //N*N    "N*N" .word's : each is 8 bytes wide and are initialized with hexadecimal value of 5
B:  
    .word 0x0
    .fill 256, 8, 0x40180000         // N*N = 128*128
C:
    .word 0x0
    .fill 256, 8, 0x0


.globl _start
_start:
BL CONFIG_VIRTUAL_MEMORY

    // Step 1-3: configure PMN0 to count cycles.
    MOV R0, #0                  //Write 0 into R0 then PMSELR
    MCR p15, 0, R0, c9, c12, 5  //Write 0 into PMSELR selects PNM0
    MOV R1, #0x11               //Event 0x11 is CPU cycles
    MCR p15, 0, R1, c9, c13, 1  //Write 0x11 into PMXEVTYPER (PMN0 measure CPU cycles.)
    MOV R0, #1                  //Write 1 into R0 then PMSELR
    MCR p15, 0, R0, c9, c12, 5  //Write 1 into PMSELR selects PNM1
    MOV R1, #0x6                //Event 0x6 is number of load instructions executed.
    MCR p15, 0, R1, c9, c13, 1  //Write 0x6 into PMXEVTYPER (PMN1 measure load instructions executed.)
    MOV R0, #2                  //Write 2 into R0 then PMSELR
    MCR p15, 0, R0, c9, c12, 5  //Write 2 into PMSELR selects PNM2
    MOV R1, #0x3                //Event 0x3 is level 1 data cache misses.
    MCR p15, 0, R1, c9, c13, 1  //Write 0x3 into PMXEVTYPER (PMN2 measure data cache misses.)
    // Step 4: enable PMNx
    MOV R0, #0b111              //PMNx is nit 0 of PMCNTENSET
    MCR p15, 0, R0, c9, c12, 1  //Setting bit 0 of PMCNTENSET enbales PMN0
    // Step 5: clear all counters and start counters
    MOV r0, #3                  //bits 0 (start counters) and 1 (reset counters)
    MCR p15, 0, r0, c9, c12, 0  //setting PMCR to 3

    //Step 6: Code we want to test:


matrix_multiply:
    // saving R4-R6 into stack:
    //PUSH {R4-R6}
    PUSH {LR}

    LDR R9, =A      // R9 =base address of array "A"
    LDR R10, =B     //  R10=base address of array "B"
    LDR R11, =C    //  R11=base address of array "C"
    MOV R0, #0      //  R0 = i = 0
    LDR R1, =N  //( address of N) loading the address of "N" into R1 
    LDR R1, [R1] //(R1= value  of N) getting the value of "N" from memory into R2
                        // R1 = N
    CMP R0, R1  // i<N 
    BLT j_loop
    B return 
                        // R3 = j

i_loop:
       
    CMP R0, R1               // i<N      
    BLT j_start
    B return

j_start:
    MOV R3, #0   // j=0  
j_loop:     
    CMP R3, R1   // j<N
    BLT j_sum    // j<N
    // i++:
    ADD R0, R0, #1 
    B i_loop
    // ***NOTE: please do not forget j++
j_sum:  
    LDR R4, =zero
       
    .word 0b11101101100101000000101100000000 // FLDD D0, [R4]  <==> D0=sum    // cannot use MOV instruction for Coprocessor registers (S0-31)

    MOV R5, #0              // R5 = k = 0 
k_loop:    
    CMP R5, R1  //  k<n
    BLT k_inside              // R1 = N
    B c_i_j

k_inside:    
    MUL R7, R5, R1      //   i*N    ("N" elements in each row and column)      
    ADD R6, R7, R0        // R6 = (i+N*k)   =   [i][k]

    //turning "i*N+k" into a byte index:
    // Remember, alpha[i] == (4*i) + base of alpha :
   
    ADD R6, R9, R6, LSL #3    // R6 = actual address of A[i][k]  (adding base address of array of "A" with [i][k] value)
    .word 0b11101101100101100001101100000000      // <==> FLDD D1, [R6,#0]              <==>    D1 = A[i][k]
    // Do not change R6
  

    // B array (B[k][j]):

    MUL R7, R1, R3  //  N*k             // R1 = N 
    ADD R7, R7, R5  // R7 = N*k+j           // R3=j
                                 //note: R10 = base address of the "B" array
    ADD R8, R10, R7, LSL #3    // R8 = (N*j+k)*8 + base of array B
    .word 0b11101101100110000010101100000000  // <==> FLDD D2, [R8, #0]   <==>   D2 = B[k][j] // should be R8
    
    //FMULD:
    .word  0b11101110001000010011101100000010 // <==> FMULD D3, D1, D2     <==>   D3 = A[i][k] * B[k][j]

    // FADDD D0, D3, D0    <=>    D0 =  A[i][k]*B[k][j] + sum
    .word 0b11101110001100110000101100000000

    // k++:
    ADD R5, R5, #1

    B k_loop


c_i_j:
    // C[i][j]              // R0=i      // R3=j
    MUL R7, R1, R3       //  N*i
    ADD R7, R7, R0      //  R7 = (N*j) + i
    ADD R2, R11, R7, LSL #3   // (N*j+i)+base of "C" array
                    //*** R2 = C[i][j]


    // FSTD D0, [R2,#0]:
    .word 0b11101101100000100000101100000000

    // j++:
    ADD R3, R3, #1  
    B j_loop


return:
    POP {LR}
    //MOV PC, LR
end:
//B end

//Step 7: stop counters
    mov r0, #0
    MCR p15, 0 , r0, c9, c12, 0 //Write 0 to PMCR to stop counters

    //Step 8-10: Select PMNx and read out result into Ry.
    mov r0, #0                  //PMN0
    MCR p15, 0, R0, c9, c12, 5  //Write 0 to PMSELR
    MRC p15, 0, R3, c9, c13, 2  //Read PMXEVCNTR into R3
    mov r0, #1                  //PMN1
    MCR p15, 0, R0, c9, c12, 5  //Write 1 to PMSELR
    MRC p15, 0, R7, c9, c13, 2  //Read PMXEVCNTR into R7
    mov r0, #2                  //PMN2
    MCR p15, 0, R0, c9, c12, 5  //Write 2 to PMSELR
    MRC p15, 0, R8, c9, c13, 2  //Read PMXEVCNTR into R8

finish:
    B finish






