N_size:   .word 16   // put the (32-bit integer) #128 into the address location "N"
zero: .double 0.0 // 8-byte wide
blocksize:  .word 16 //block size

A_array:
    .word 0x0
    .fill 256, 8, 0x40140000 //N*N  //    "N*N" .word's : each is 8 bytes wide and are initialized with hexadecimal value of 5
B_array:  
    .word 0x0
    .fill 256, 8, 0x40180000 //16384 for 128 * 128
C_array:
    .word 0x0 
    .fill 256, 8, 0x0

.global _start

_start:
BL CONFIG_VIRTUAL_MEMORY

    // Step 1-3: configure PMN0 to count cycles.
    MOV R0, #0                  //Write 0 into R0 then PMSELR
    MCR p15, 0, R0, c9, c12, 5  //Write 0 into PMSELR selects PNM0
    MOV R1, #0x11               //Event 0x11 is CPU cycles
    MCR p15, 0, R1, c9, c13, 1  //Write 0x11 into PMXEVTYPER (PMN0 measure CPU cycles.)
    MOV R0, #1                  //Write 1 into R0 then PMSELR
    MCR p15, 0, R0, c9, c12, 5  //Write 1 into PMSELR selects PNM1
    MOV R1, #0x6                //Event 0x6 is number of load instructions executed.
    MCR p15, 0, R1, c9, c13, 1  //Write 0x6 into PMXEVTYPER (PMN1 measure load instructions executed.)
    MOV R0, #2                  //Write 2 into R0 then PMSELR
    MCR p15, 0, R0, c9, c12, 5  //Write 2 into PMSELR selects PNM2
    MOV R1, #0x3                //Event 0x3 is level 1 data cache misses.
    MCR p15, 0, R1, c9, c13, 1  //Write 0x3 into PMXEVTYPER (PMN2 measure data cache misses.)
    // Step 4: enable PMNx
    MOV R0, #0b111              //PMNx is nit 0 of PMCNTENSET
    MCR p15, 0, R0, c9, c12, 1  //Setting bit 0 of PMCNTENSET enbales PMN0
    // Step 5: clear all counters and start counters
    MOV r0, #3                  //bits 0 (start counters) and 1 (reset counters)
    MCR p15, 0, r0, c9, c12, 0  //setting PMCR to 3

    //Step 6: Code we want to test:
matrix_multiply:
    MOV R0, #0  // i=0
    LDR R4, =N_size  //(R1= address of N) loading the address of "N" into R1 
    LDR R4, [R4] //(R2= value  of N) getting the value of "N" from memory into R2
i_loop:
    CMP R0, R4  // i<N 
    BLT j_start
    B end

j_start:
    MOV R1, #0   // j=0
j_loop:
    CMP R1, R4  // j<N
    BLT k_start       // j<N
    ADD R0, R0, R4
    B i_loop

k_start:
    MOV R2, #0  // k=0 
k_loop:
    CMP R2, R4  // k<n
    BLT k_inside
    ADD R1, R1, R4
    B j_loop

k_inside:
    BL  do_block
    ADD R2, R2, R4
    B   k_loop

end: 
    //B end

//Step 7: stop counters
    mov r0, #0
    MCR p15, 0 , r0, c9, c12, 0 //Write 0 to PMCR to stop counters

    //Step 8-10: Select PMNx and read out result into Ry.
    mov r0, #0                  //PMN0
    MCR p15, 0, R0, c9, c12, 5  //Write 0 to PMSELR
    MRC p15, 0, R3, c9, c13, 2  //Read PMXEVCNTR into R3
    mov r0, #1                  //PMN1
    MCR p15, 0, R0, c9, c12, 5  //Write 1 to PMSELR
    MRC p15, 0, R7, c9, c13, 2  //Read PMXEVCNTR into R7
    mov r0, #2                  //PMN2
    MCR p15, 0, R0, c9, c12, 5  //Write 2 to PMSELR
    MRC p15, 0, R8, c9, c13, 2  //Read PMXEVCNTR into R8

finish:
    B finish

do_block:
    PUSH {R0-R12,LR}
    MOV R3, R0
    LDR R7, =blocksize
    LDR R7, [R7]
    LDR R12, =N_size
    LDR R12, [R12]
    //R1: sj, R2: sk, R3: si
    //R4: j, R5: k, R6: i
L1_start:
    MOV R6, R3 //i = si
L1:
    ADD R8, R7, R3 //si+blocksize
    CMP R6, R8 //i < si+blocksize
    BLT L2_start
    B return_part

L2_start:
    MOV R4, R1 //j = sj
L2:
    ADD R8, R7, R1 //sj+blocksize
    CMP R4, R8
    BLT L2_cij
    ADD R6, R6, #1
    B L1

L2_cij:
    MUL R8, R4, R12
    ADD R9, R8, R6
    LDR R10, =C_array
    ADD R10, R10, R9, LSL #3
    .word 0b11101101100110100000101100000000 //FLDD D0, [R10]

L3_start:
    MOV R5, R2 //k = sk
L3:
    ADD R8, R7, R2
    CMP R5, R8
    BLT body
    B put_cij

body:
    MUL R8, R5, R12
    ADD R9, R8, R6
    LDR R8, =A_array
    ADD R8, R8, R9, LSL #3 //A[i+k*n] address
    .word 0b11101101100110000001101100000000 //FLDD D1, [R8]
    MUL R8, R4, R12
    ADD R9, R8, R5
    LDR R8, =B_array
    ADD R8, R8, R9, LSL #3 //B[k+j*n] address
    .word 0b11101101100110000010101100000000 //FLDD D2, [R8]
    .word 0b11101110001000010011101100000010 //FMULD D3, D1, D2
    .word 0b11101110001100000000101100000011 //FADD D0, D0, D3
    ADD R5, R5, #1
    B L3

put_cij:
    .word 0b11101101100010100000101100000000 //FSTD D0, [R10]
    ADD R4, R4, #1
    B L2

return_part:
    POP {R0-R12, LR}
    BX LR

